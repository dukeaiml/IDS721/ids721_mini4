use axum::{
    extract::Path,
    http::StatusCode,
    response::Json,
    routing::get,
    Router,
};
use tokio;
use serde::Serialize;

#[derive(Serialize)]
struct User {
    id: u64,
    name: String,
    email: String,
}

async fn get_user(Path(id): Path<u64>) -> Json<User> {
    let user = User {
        id,
        name: "James".to_string(),
        email: "james@example.com".to_string(),
    };
    Json(user)
}

#[tokio::main]
async fn main() {
    let app = Router::new()
                    .route("/", get(|| async { "Hello, World!" }))
                    .route("/user/:id",get(get_user));

    // run our app with hyper, listening globally on port 3000
    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}