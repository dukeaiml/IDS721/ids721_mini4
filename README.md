# IDS721_mini4



### A simple Rust Axum Web App
![Alt text](image-4.png)


### Distroless Dockerfile

![Alt text](image-3.png)

### Build the docker image 

```bash
docker build -t <appname> .
```

![Alt text](image.png)


### Run the app

```bash
docker run -p port:port <appname>
```

![Alt text](image-1.png)

send the request:

```
 curl localhost:3000/user/30 
```


![Alt text](image-2.png)


here we can get a response from the host(a struct data format in json)